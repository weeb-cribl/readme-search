import requests

### This script uses the readme.io API to search docs.cribl.io documentation
### for a given search string from the latest version "v2.1" from the user
### account for Louise Tang
### - L Tang 02/14/2020

### Method: POST
### URL: https://dash.readme.io/api/v1/docs/search?search=distributed
### Request headers: x-readme-version: v2.1
### Authorization: Basic bmZFZ0pwcUh4bUJiYTdmTXJrWkc5cmlUcWZrcTd2U2c6

### Response headers
### access-control-allow-origin: *
### cache-control: no-cache, no-store, must-revalidate
### connection: keep-alive
### content-encoding: gzip
### content-type: application/json; charset=utf-8
### date: Sat, 15 Feb 2020 06:20:29 GMT
### etag: W/"18bd3-6kbdUKGCm4o0wEwnLgU+S/4g2ZE"
### server: nginx/1.13.1
### strict-transport-security: max-age=15552000; includeSubDomains
### transfer-encoding: Identity
### vary: X-HTTP-Method-Override, Accept-Encoding
### via: 1.1 vegur, 1.1 vegur
### x-content-type-options: nosniff
### x-dns-prefetch-control: off
### x-download-options: noopen
### x-frame-options: Deny
### x-powered-by: Express
### x-xss-protection: 1; mode=block

url = "https://dash.readme.io/api/v1/docs/search"

querystring = {"search":"distributed"}

headers = {
    'x-readme-version': "v2.1",
    'authorization': "Basic bmZFZ0pwcUh4bUJiYTdmTXJrWkc5cmlUcWZrcTd2U2c6"
    }

response = requests.request("POST", url, headers=headers, params=querystring)

print(response.text)